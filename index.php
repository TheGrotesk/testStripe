<?php

    require_once('vendor/autoload.php');

    \Stripe\Stripe::setApiKey('sk_test_cM1mUTi630jGpvbFoRhMhD3100hVEbMq8U');

    $app = new \Slim\App(
        [
            'settings' => [
                'displayErrorDetails' => true
            ]
        ]
    );

    $app->group('/stripe', function() use ($app){

            $app->group('/subscription', function() use ($app){
                    $app->post('/customer/create', function($rquest, $response, $args){
                        $params = $rquest->getParsedBody();
                       
                        $check = \Stripe\Customer::all(["email" => $params['email']]);

                        if($check['data'] !== NULL && $check['data']['0'] !== NULL){
                            return $response->withHeader('Content-Type', 'application/json')
                                    ->write(json_encode([

                                        'status' => 'error',
                                        'message' => 'email already in use!'

                                    ]));
                        }
                        
                        try{
                            $customer = \Stripe\Customer::create([
                                "description" => $params['description'],
                                "email" => $params['email'],
                                "source" => $params['token']
                            ]);
                        }catch(\Stripe\Stripe_InvalidRequestError $e){
                            return json_encode([
                                'status'=>'error',
                                'message' => $e->getMessage()
                            ]);
                        }catch (Stripe_Error $e) {
                            return json_encode([
                                'status'=>'error',
                                'message' => $e->getMessage()
                            ]);
                        }

                        if($customer){
                            return $response->withHeader('Content-Type', 'application/json')
                            ->write($customer);
                        }  
                    });

                    $app->group('/plans', function() use ($app){
                            $app->get('/', function($request, $response, $argv){
                                $params = $request->getParsedBody();

                                $plans = \Stripe\Plan::retrieve($params['productId']);
                            });
                    });
            });
    });

    $app->run();

?>